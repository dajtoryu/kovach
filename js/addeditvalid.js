    
    $(document).ready(function(){
      
$("#productnew").validate({
    normalizer: function(value) {
    return $.trim(value);
  },
  ignore:[],

                rules:{
                    sku:{required:true},
                    name:{minlength:4},
                    price:{required:true,number:true},
                    qty:{number:true, required:true},
                    description:{minlength:10,required: function(textarea) {
          CKEDITOR.instances[textarea.id].updateElement(); // update textarea
          var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
          return editorcontent.length === 0;
        }}
                },
                messages:{
                    sku:{required:"поле не може бути пустим"},
                    name:{minlength:"мінімум 4 символи",required:"поле не може бути пустим, мінімум 4 символи"},
                    price:{number:"тільки число без коми",required:"поле не може бути пустим"},
                    qty:{number:"тільки число без коми",required:"поле не може бути пустим"},
                    description:{minlength:"мінімально 10 символів",required:"поле не може бути пустим, мінімум 10 символів"}
                },
        });

  

})


