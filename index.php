<?php
error_reporting(E_ALL);
ini_set('display_startup_errors', 1);
ini_set('display_errors', '1'); 
function debug($arr){
    echo "<pre>";
    print_r($arr);
    echo "</pre>";
}
define("ROOT", getcwd());
define("DS", DIRECTORY_SEPARATOR);
include ROOT . '/app/bootstrap.php';
