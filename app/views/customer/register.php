<h4>На цій сторінці відключена будь-яка валідація на фронті</h4>
<?php if(isset($errors)):?>
 <?php foreach($errors as $v):?>
<p class="error"><?=$v;?></p>

<?php endforeach;?>
<?php endif;?>
<form method="post">
  <div class="form-group">
    <label for="exampleInputEmail1">Імя</label>
    <input type="text" class="form-control" id="" aria-describedby="emailHelp" placeholder="Імя" name="first_name" value="<?php echo Core\Helper::oldInput('first_name');?>">
    <small id="emailHelp" class="form-text text-muted">Тільки букви, не менше 2 символів</small>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Прізвище</label>
    <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Прізвище" name="last_name" value="<?php echo Core\Helper::oldInput('last_name');?>">
        <small id="emailHelp" class="form-text text-muted">Тільки букви, не менше 2 символів</small>

  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Телефон</label>
    <input type="text"  id="phone" class="form-control" id="exampleInputPassword1" placeholder="Телефон" name="phone" value="<?php echo Core\Helper::oldInput('phone');?>">
        <small id="emailHelp" class="form-text text-muted">Цифри у форматі +389999999999</small>

  </div>
    <div class="form-group">
    <label for="exampleInputPassword1">Місто</label>
    <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Місто" name="city" value="<?php echo Core\Helper::oldInput('city');?>">
        <small id="emailHelp" class="form-text text-muted">Тільки букви, не менше 2 символів</small>

  </div>
    <div class="form-group">
    <label for="exampleInputPassword1">Емейл</label>
    <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Емейл" name="email" value="<?php echo Core\Helper::oldInput('email');?>">

  </div>
    <div class="form-group">
    <label for="exampleInputPassword1">Пароль</label>
    <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Пароль" name="password" value="<?php echo Core\Helper::oldInput('password');?>">
        <small id="emailHelp" class="form-text text-muted">Англійські букви і цифри, не менше 8 символів</small>

  </div>
     <div class="form-group">
    <label for="exampleInputPassword1">Підтвердження пароля</label>
    <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Підтвердіть пароль" name="confirmpass" value="<?php echo Core\Helper::oldInput('confirmpass');?>">

  </div>
  <button type="submit" class="btn btn-primary">Реєстрація</button>
</form>
<script src="/libs/jquery.maskedinput.min.js"></script>
<script>  
$(document).ready(function(){
  $("#phone").mask("+389999999999");
});
</script>