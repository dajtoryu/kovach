<?php if(isset($_GET['s']) ):?>
<div class="alert alert-success saved" role="alert">
    <p>Продукт <?=$_GET['s'];?> збережено вдало</p>
</div>
<?php endif;?>
<?php $errors= $this->get('errors');?>
<?php if($errors !== null):?>
<div class="alert alert-danger">
<?php foreach($errors as $v):?>
    <p><?=$v;?></p>
<?php endforeach;?>
    </div>
<?php endif;?>
<h4>Редагування товару <b><?=$product['name'];?></b></h4>
<table class="table">
  <thead>
    <tr>
      <th scope="col">SKU</th>
      <th scope="col">Назва</th>
      <th scope="col">Ціна</th>
      <th scope="col">Кількість</th>
      

    </tr>
  </thead>
  <tbody>
  <form id="editf" action="" method="post">
    <tr>
        <td scope="row"><input type="text" value="<?=$product['sku'];?>" id="sku"    name="sku"  required ><br><small>Поле не може бути пустим</small> </td>
      <td scope="row"><input type="text" class="formname" value="<?=$product['name'];?>"  id="name"  name="name" required><br><small>Поле не може бути пустим</small> </td>
      <td scope="row"><input type="text" value="<?=$product['price'];?>" id="price" name="price" required><br><small>Ціле число або через крапку</small> </td>
      <td scope="row"><input type="text" value="<?=$product['qty'];?>" name="qty" required><br><small>Ціле число або через крапку</small></td>
    <input type="hidden" name="id" value="<?=$product['id'];?>">
      
    </tr>
    <tr><th scope="col" colspan="4">Описання товару<br><small>Для описання товару мінімум 10 знаків</small></th></tr>   
    <tr><td colspan="4">
            <textarea name="description"  required> <?=$product['description'];?></textarea>
      
      </td></tr>
    <tr><td colspan="4">
            <button type="submit" class="btn btn-primary">Зберегти товар</button> </form>
            <form class="delprod" action="<?= Core\Route::getBP()."/product/delete?id={$product['id']}";?>" method='post'>
                <button type="submit" class="btn btn-danger">Видалити товар</button> <input type="hidden" value="<?=$product['id'];?>"></form>
                <form class="backform" method="post" action="<?=Core\Route::getBP()."/product/back";?>"> 
                    <button type="submit" class="btn btn-success">Повернутись до товарів</button></form>
           
        </td>
    </tr>
  
  </tbody>
</table>

<script src="/libs/ckeditor/ckeditor.js"></script>
<script src="/libs/jquery.validate.min.js"></script>
<script>
    window.onload = function() {
       CKEDITOR.replace( 'description' );
    };
</script>
<script>
    $(document).ready(function(){
      
$("#editf").validate({
    normalizer: function(value) {
    return $.trim(value);
  },
  ignore:[],
  
                rules:{
                    sku:{required:true},
                    name:{minlength:4},
                    price:{required:true,number:true},
                    qty:{number:true, required:true},
                    description:{minlength:10,required: function(textarea) {
          CKEDITOR.instances[textarea.id].updateElement(); // update textarea
          var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
          return editorcontent.length === 0;
        }}
                },
                messages:{
                    sku:{required:"поле не може бути пустим"},
                    name:{minlength:"мінімум 4 символи",required:"поле не може бути пустим, мінімум 4 символи"},
                    price:{number:"тільки число без коми",required:"поле не може бути пустим"},
                    qty:{number:"тільки число без коми",required:"поле не може бути пустим"},
                    description:{minlength:"мінімально 10 символів",required:"поле не може бути пустим, мінімум 10 символів"}
                },
        });

  

})


</script>

<script>
    
    $(document).ready(function() {
  $('.delprod').submit(function (evt) {
    
    
   var value=$('.formname').attr('value');
   var res="Ви дійсно хочете видалити товар "+value+"?";
   var answer=confirm(res);
   if(!answer){
     evt.preventDefault();
   }
});
  });


</script>
