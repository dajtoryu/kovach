<?php $admin=$this->get('admin');?>
<form method="POST" action="<?php $_SERVER['PHP_SELF']; ?>">
<select name='sortfirst'>
    <?php $selected=$this->get('selected');?>
    <option <?php echo $selected['price']['ASC'] ;?> value="price_ASC">від дешевших до дорожчих</option>
    <option <?php echo $selected['price']['DESC'];?> value="price_DESC">від дорожчих до дешевших</option>
</select>
<select name='sortsecond'>
  <option <?php echo $selected['qty']['ASC'];?>  value="qty_ASC">по зростанню кількості</option>
  <option <?php echo $selected['qty']['DESC'];?>  value="qty_DESC">по спаданню кількості</option>
</select>

<h4>Фільтр за ціною </h4>

Ціна від:<input type="text" name="pricefrom" > до:<input type="text"  name="priceto" value="<?= $this->get('maxprice');?>">
<input type="submit" value="Submit">
</form>
<?php if($admin):?>
<div class="product"><p>
        <?= \Core\Url::getLink('/product/add', 'Додати товар'); ?>
</p></div>
<?php endif;?>

<?php

$products =  $this->get('products');

foreach($products as $product)  :
?>
    <div class="product">
        <p class="sku">Код: <?php echo $product['sku']?></p>
        <h4><?php echo $product['name']?><h4>
        <p> Ціна: <span class="price"><?php echo $product['price']?></span> грн</p>
        <p> Кількість: <?php echo $product['qty']?></p>
        <p><?php if(!$product['qty'] > 0) { echo 'Нема в наявності'; } ?></p>
        <?php if($admin):?>
        <p>
            <?= \Core\Url::getLink('/product/edit', 'Редагувати', array('id'=>$product['id'])); ?>
        </p>
        <?php endif;?>
        <a href="/product/view?id=<?=$product['id'];?>">Переглянути</a> 
    </div>

<?php endforeach; ?>


