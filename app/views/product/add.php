
<h4>Ввести новий товар</h4>
<?php if(isset($msg)) echo $msg;?>
<?php if(isset($errors)):?>
 <?php foreach($errors as $v):?>
<p class="error"><?=$v;?></p>

<?php endforeach;?>
<?php endif;?>
<form id="productnew" method="POST" action="">
  <div class="form-group">
      <label for="sku">SKU:</label>  <br>  <small id="emailHelp" class="form-text text-muted">Поле не може бути пустим,</small>

      <input type="text" class="form-control" id="sku"  placeholder="sku введіть"  name="sku"  value="<?php echo Core\Helper::oldInput('sku');?>">
  </div>
  <div class="form-group">
      <label for="name">Назва товару</label>  <br>  <small id="emailHelp" class="form-text text-muted">Поле не може бути пустим, не менше чотирьох символів</small>

    <input type="text" class="form-control" id="name" placeholder="Назва товару" name="name"  value="<?php echo Core\Helper::oldInput('name');?>"required>
  </div>
  <div class="form-group">
    <label for="price">Ціна</label> <br><small id="emailHelp" class="form-text text-muted">Ціле число  або  через крапку</small>
    <input type="text" class="form-control" id="price" placeholder="ціна" name="price"  value="<?php echo Core\Helper::oldInput('price');?>"required>
  </div>
    <div class="form-group">
        <label for="qty">Кількість</label> <br>   <small id="emailHelp" class="form-text text-muted">Ціле число  або  через крапку</small>

    <input type="text" class="form-control" id="qty" placeholder="Кількість" name="qty"  value="<?php echo Core\Helper::oldInput('qty');?>" required>

  </div>
     
    <div class="form-group">
        <label for="desc">Описання товару</label>    <br> 
<small id="emailHelp" class="form-text text-muted">Мінімум 10 символів</small>
    <textarea style="resize:none;" class="form-control" id="desc"   rows="15" name="description" required><?php echo Core\Helper::oldInput('description');?></textarea>
  </div>
  <button type="submit" name="enter" class="btn btn-primary">Зберегти</button>
</form>


<script src="/libs/jquery.validate.min.js"></script>
<script src="/libs/ckeditor/ckeditor.js"></script>
<script>
       CKEDITOR.replace( 'description' );

        </script>
   
        
       

        <script>
    $(document).ready(function(){
      
$("#productnew").validate({
    normalizer: function(value) {
    return $.trim(value);
  },
  ignore:[],

                rules:{
                    sku:{required:true},
                    name:{minlength:4},
                    price:{required:true,number:true},
                    qty:{number:true, required:true},
                    description:{minlength:10,required: function(textarea) {
          CKEDITOR.instances[textarea.id].updateElement(); // update textarea
          var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
          return editorcontent.length === 0;
        }}
                },
                messages:{
                    sku:{required:"поле не може бути пустим"},
                    name:{minlength:"мінімум 4 символи",required:"поле не може бути пустим, мінімум 4 символи"},
                    price:{number:"тільки число без коми",required:"поле не може бути пустим"},
                    qty:{number:"тільки число без коми",required:"поле не може бути пустим"},
                    description:{minlength:"мінімально 10 символів",required:"поле не може бути пустим, мінімум 10 символів"}
                },
        });

  

})


</script>
