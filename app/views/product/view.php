
<?php $products=$this->get('products');

?>
<?php if($products !== null):?>

<div class="card">
  <h4 class="card-header"><?=$products['name'];?></h4>
  <div class="card-body">
      Ціна:<h5 class="card-title"><?=$products['price'];?></h5>
      Опис:<p class="card-text"><?= html_entity_decode($products['description']);?></p>
    <a href="/product/list" class="btn btn-primary">Назад</a>
  </div>
</div>

<?php else:?>
    <h3>Такого товара не існує</h3>
    <?php endif;?> 
   
