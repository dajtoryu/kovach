<?php
namespace Models;

use Core\Model;
use Core\DB;

/**
 * Class Product
 */
class Product extends Model
{
    
    

    /**
     * Product constructor.
     */
    function __construct()
    {
        $this->table_name = "products";
        $this->id_column = "id";
    }
    

    public  function addItem($values){
       
        //якщо ключі  із POST відповідають назвам колонок
        $cols=array_keys($values);
        if($this->validColumNames($cols)){
            
            $columns= implode(',', array_keys($values));//формуємо строку для запису такого типу:  sku,name,price
        //
        //для підготовленого запиту сформуємо строку виду: ?,?,? де кількість знаків питання дорівнює кількості колонок для запису
        $str= str_repeat('?,',count($values));  
        $str_len = strlen($str);
        $str=substr($str,0,$str_len-1);
        
        // підготовлена строка запросу виду: INSERT INTO products (sku,name...) VALUES (?,?,...)
       $this->sql = "INSERT INTO ".$this->table_name. " ($columns) VALUES  ($str)";
      //сформувати масив значень для передачі як параметра в підготовлений запит
       $prepare_values= array_values($values);
       
       $db = new DB();
      $res= $db->insertQuery($this->sql, $prepare_values);
      return $res;
        
        
        
    }
    
        }
    
    //отримати кількість записів в колекції значень
    public  function getCount(){
        
     
        return count($this->collection);
        
    }
    
//    //перевизначений батьківський метод для можливості пагінації
//    //за умовчанням в аргументи передаємо масив виду [x,y]  що відповідає sql "limit x,y"
//        public function getCollection($lim=[])
//    {
//        if(!empty($lim)){
//                    $this->sql .= " limit ${lim[0]},${lim[1]};";
//
//            
//        }
//               $db = new DB();
//        echo $this->sql;
//        $this->collection = $db->query($this->sql, $this->params);
//        return $this;
//    }
    
    public function saveItem($id,$values){
        $cols=array_keys($values);
       $sql = "UPDATE  {$this->table_name} SET ";
          $params=[];  
           foreach($values as $k=>$v){
               $sql.="$k=?,";
               $params[]=$v;
           }
           
        $str_len = strlen($sql);
        $sql=substr($sql,0,$str_len-1);
        $sql.=" WHERE id=?";
        $params[]=$id;
        

       $db = new DB();
      $res= $db->query($sql, $params);
      return $res;
        
        
        
    
    }
    
    public function deleteItem(){
          $db = new DB();
          $db->deleteEntity($this);
    }
    
   public function getMaxPrice(){
       $sql="select max(price) p from ".$this->table_name.";";
       $db= new DB();
        $res=$db->query($sql);
     
        return (int) $res[0]['p'];
   }
   
}