<?php

namespace Models;
use Core\Model;
use Core\DB;
class Customer extends Model{
    function __construct()
    {
        $this->table_name = "customers";
        $this->id_column = "id";
    }
    
    
        public function filter($params)
    {


$sql=" WHERE ";
foreach($params as $k=>$v){
    $sql.="${k} =? and ";
}

$len= mb_strlen($sql);
$sql= mb_substr($sql, 0,$len-4);
$this->sql.=$sql;
$this->params= array_values($params);
              return $this;
        
        
    }
    
    public  function addItem($values){
       
       
        
            
            $columns= implode(',', array_keys($values));//формуємо строку для запису такого типу:  sku,name,price
        //
        //для підготовленого запиту сформуємо строку виду: ?,?,? де кількість знаків питання дорівнює кількості колонок для запису
        $str= str_repeat('?,',count($values));  
        $str_len = strlen($str);
        $str=substr($str,0,$str_len-1);
        
        // підготовлена строка запросу виду: INSERT INTO customers (first_name,last_name...) VALUES (?,?,...)
       $this->sql = "INSERT INTO ".$this->table_name. " ($columns) VALUES  ($str)";
      //сформувати масив значень для передачі як параметра в підготовлений запит
       $prepare_values= array_values($values);
       
       $db = new DB();
      $res= $db->insertQuery($this->sql, $prepare_values);
      return $res;
        
        
        
    
    
        }
        
        public function getSales($id)
    {
            $sql="select p.name as pn, p.price as pp, so.datetime as sd, sor.qty  as sq from sales_order so join sales_orderitem sor
on so.order_id=sor.order_id join products p on p.id=sor.product_id where so.customer_id=?";
       
        $db = new DB();
        $params = array($id);
         return $db->query($sql, $params);
    }
}
