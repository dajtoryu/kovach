<?php
namespace Core;

/**
 * Class Controller
 */
class Controller
{
    protected $data = [];

    public function __construct()
    {
        $this->set('layoutPath', App::getLayoutDir() . DS. 'layout.php');
        $this->set('menuPath', App::getViewDir() . DS. 'menu.php');
    }

    protected function set($key, $value)
    {
        $this->data[$key] = $value;
    }

    protected function get($key)
    {
        if(array_key_exists($key,$this->data)){
            return $this->data[$key];
        }
        
        return null;
    }

    public function renderLayout()
    {
        $this->set('menuCollection',$this->getMenuCollection());
        $menu =  new View($this->data, $this->get('menuPath'));

        $content = new View($this->data);

        $this->set('menu', $menu);
        $this->set('content', $content);

        $view = new View($this->data, $this->get('layoutPath'));

        echo $view->render();
    }

     /**
     * @param $name
     * @return mixed
     */
    public function getModel($name)
    {
        $name = '\\Models\\' . ucfirst($name);
        $model = new $name();
        return $model;
    }

     /**
     * @return mixed
     */
     private function getMenuCollection()
     {
        return $this->getModel('menu')
            ->initCollection()
            ->sort(array('name'=>'DESC'))
            ->getCollection()
            ->select();
     }

     protected function forward($route)
     {
         App::run($route);
     }
     
     //якщо визов без параметрів, то переадресація або на попередню сторінку( для реєстрації-входу), або якщо 
     //не було переходу з якоїсь сторінки, то на головну
     public  function redirect($route=null)
    {
        $server_host = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'];
        $url = $server_host . route::getBP();
        if($route !== null){
            $url.=$route;
        }else{
            $url = isset($_SERVER['HTTP_REFERER']) ?  $_SERVER['HTTP_REFERER']: $url;
        }
        header("Location:$url");
        die;
    }
    protected function setOldInput($param) {
        foreach($param as $k=>$v){
                    $_SESSION['oldinput'][$k]=$v;

        }
    }
    
    //очищає від тегів, залишає дозаолені тегі в $params $exclude - масив ключів, в елементах яких не потрібно нічого вирізати
    public  function cleanPostValues($values,$params,$exclude=[]){
        $prepare_values=[];
        foreach($values as $k=>$v){
            //якщо є ключ який не потрібно обрізати,  добавити його в масив і пропустити ітерацію
            if(!empty($exclude) &&  in_array($k, $exclude)){
                $prepare_values[$k]=$v; continue;
            }   
             
            
            //якщо в параметрах є правило для тегів, а також якщо в $values є такий ключ, оскільки getPostValues() не поверне ключі-значення пустих полів 
            if(array_key_exists($k, $params) && array_key_exists($k, $values)){
                //примінити правило дозволених тегів до таких полів
                $prepare_values[$k]= strip_tags($v,$params[$k]);
            }else{//якщо правила для поля не встановлено, то вирізати всі теги
                $prepare_values[$k]= strip_tags($v);
                
            }
            
        }
        return $prepare_values;
        
    }
    
}