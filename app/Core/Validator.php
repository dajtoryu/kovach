<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * 
 */

//приклад визову валідатора:
/*$data=['name'=>'asdЯч1yuyu','mail'=>'jklj@yu'];
$rules=['name'=>'letters','mail'=>'mail'];
$val= new Validator($arr,$rules);
if(!$val->run()){
  $errors=$val->getErrors();
}
 * */
 
namespace Core;

class Validator{
	private $data=[];
    private $rules=[];
    private $errors=[];
    private  $aliases=[];




    public function __construct($data,$rules,$aliases){
   	$this->data=$data;//вхідний масив
   	$this->rules =$rules;//масив прави валідації для вхідного масиву
                        $this->aliases=$aliases;//допоміжний, інформація про назву поля в формі, яке не пройшло валідацію
   }

    

    public function run(){
        //якщо нема тих полів, які слід записати в БД, оскільки метод model::getPostValues(), не повертає значення пусті, то добавити в помилки
        //що такі поля не можуть бути пустими
          $this->preprocess();
          //пройтись по вхідному масиву
          foreach($this->data as $k=>$v){
          	//якщо в масиві з правилами існує такий ключ, як у вхідному масиві, іншими словами, якщо для елемента вхідного масива існує правило(правила) перевірки,
          	if(array_key_exists($k, $this->rules)){
                 
                 //отримати масив із правилами для конкретного елемента первірки
          		$funcArr=explode('|',$this->rules[$k]);
                    
                 //пройтись по ньому, якщо в правилі є аргумент(напр. min:5, що значить визвати функцію min із аргументом 5), то визвати функцію із ним, якщо ні, то визвати функцію без аргумента. Для формування масиву із помилками першим аргументом у всі  методи передаємо ключ вхідного масиву(щоб було ясно по якому полю помилка), другим саме значення вхідного масиву, а третім, якщо є, то додатковий аргумент    
          		foreach($funcArr as $kf=>$func){
                            
          			if(preg_match('#:#', $func)){
          				$pos=strpos($func, ":");
                          
	             $len=substr($func,$pos+1);
	             $func=substr($func, 0,$pos);
	             
	             $this->$func($k,$v,$len);
          			}else{
          				$this->$func($k,$v);
          			}
          		}
          		
          	}
          }
          
          //якщо є помилки, метод поверне false
          return empty($this->errors);
    }

    public function getErrors(){
    	return $this->errors;
    }
    
    private function equalto($key,$data,$equal){
        $eq=$this->data[$equal];
        if(strcmp ($eq,$data) !== 0){
            $this->errors[$key]="Поле {$this->aliases[$key]} і {$this->aliases{$equal}} не співпадають";
        }
    }
   
    private function regex($key,$data,$pattern){
        if(!preg_match("#$pattern#", $data)){
            $this->errors[$key]="Поле {$this->aliases[$key]} не відповідає заданим умовам";
        }
    }
    
    
    private function req($key,$data) {
        if(mb_strlen($data) == 0){
            $this->errors[$key]=" поле {$this->aliases[$key]} не може бути пустим";
        }
    }
    private  function min($key,$data,$len){
              if(mb_strlen($data) < $len){
               $this->errors[$key]="Довжина  поля  {$this->aliases[$key]}  повинна бути мінімум $len символів";
              }
    }

    private function max($key,$data,$len){
    	if(mb_strlen($data) > $len){
              	$this->errors[$key]="Довжина  поля {$this->aliases[$key]}  повинна бути максимум $len символів";
              }
    }

    private function int($key,$data){
        var_dump($data);
         if(!is_int($data)){
         	$this->errors[$key]="Поле {$this->aliases[$key]}  повинно бути ціле число";
         }
    }

    private function float($key,$data){
              if(!is_float($data)){
         	$this->errors[$key]="Поле {$this->aliases[$key]}  повинно бути числом з  крапкою";
         }
    }


    private function email($key,$data){
//          $pattern="#[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[a-zA-Z]{2,5}#";
//          if(!preg_match($pattern, $data)){
//          	$this->errors[$key]="Не валідний адрес електронної пошти";
//          }
    
        if(!filter_var($data,FILTER_VALIDATE_EMAIL)){
            $this->errors[$key]="Не валідний адрес електронної пошти";
        };

        
    }

    private function letters($key,$data){
    	$pattern="#^[a-zA-ZабвгдеєжзиіїйклмнопрстуфхцчшщюяьАБВГДЕЭЖЗИЇЙКЛМНОПРСТУФХЦЧШЩЮЯЬ]+$#";
    	if(!preg_match($pattern, $data)){
          	$this->errors[$key]="Поле {$this->aliases[$key]} Повинно містити тільки букви без пробілів";
          };
    }
    
    private function number($key,$data){
        if(!preg_match('#^[0-9]+\.?[0-9]*$#', $data)){
            $this->errors[$key]="  Поле {$this->aliases[$key]}  дозволяється лише ціле число або з крапкою";
        }
    }
    
    

    //методи відрізняється від інших методів  довжин тим, що перевіряє довжину символів, які приходять з текстових полів
    //без тегів
    private function mintextlen($key,$data,$len){
       $text=strip_tags($data);
       $this->min($key,$text,$len);
    }

    private function maxtextlen($key,$data,$len){
        $text=strip_tags($data);
        $this->max($key,$text,$len);
    }
    
    


    
    
    private function preprocess(){
          foreach($this->rules as $k=>$v){
              if(!array_key_exists($k, $this->data)){
                  $this->errors[$k]="поле {$this->aliases[$k]} не може бути пустим";
              }
          }
        
    }




}