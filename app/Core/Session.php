<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core;

/**
 * Description of Session
 *
 * @author miha
 */
class Session {
    
    public static function set($key,$value){
        $_SESSION[$key]=$value;
    }
    
    public static function get($key) {
        if(array_key_exists($key, $_SESSION)){
            return $_SESSION[$key];
        }else{
            return null;
        }
    }
    
    public static function destroy($key,$all=false){
        if($all){
            session_destroy();
            return false;
        }
        if(array_key_exists($key, $_SESSION)){
            unset($_SESSION[$key]);
            return true;
        }
        
        return false;
    }
}
