<?php
namespace Core;

/**
 * Class Model
 */
class Model implements DbModelInterface
{
    /**
     * @var
     */
    protected $table_name;
    /**
     * @var
     */
    protected  $id=[];//введено для роботи  метода DB::deleteEntity
    protected $id_column;//назва колонки яка є  первинним ключом
    /**
     * @var array
     */
    protected $columns = [];
    /**
     * @var
     */
    protected $collection;
    /**
     * @var
     */
    protected $sql;
    /**
     * @var array
     */
    protected $params = [];

    protected $sortorder=[];
    /**
     * @return $this
     */
    public function initCollection()
    {
        $columns = implode(',',$this->getColumns());
        $this->sql = "select $columns from " . $this->table_name ;
        return $this;
    }

    /**
     * @return array
     */
    public function getColumns()
    {
        $db = new DB();
        $sql = "show columns from  $this->table_name;";
        $results = $db->query($sql);
        foreach($results as $result) {
            array_push($this->columns,$result['Field']);
        }
        return $this->columns;
    }


    /**
     * @param $params 
     * 
     * @return $this
     */
    public function sort($params=[])
    {
        
        if(!empty($params)){
        //якщо назви стовпчиків для сортування відповідають назві їх в таблиці БД та ASC/DESC лише  в такому випадку сортуємо
        if($this->validSortParams($params)){
            $this->sql.=" order by  ";
            $sql="";
            foreach($params as $k=>$v){
                
                $sql.="$k $v,";
            }
            
            //відрізаємо лишню задню кому
            $len= strlen($sql);
            $sql = substr($sql,0,$len-1);
            $this->sql.=$sql;
 
        }
        }
 
            
        
       /*
              TODO
              return $this;
        */
        return $this;
    }

    /**
     * @param $params
     */
    //на вхід масив виду: [0=>'price',1=>['>='=>'цінавід','<='=>'цінадо']], логіка під вираз and або без такого, якщо необхідний фільтр по одному
    // полю, наприклад: [0='id',1=>['='=>23]]. Недоліком є неможливість примінити or а також різні поля Тому, на мою думку, краще реалізовувати свій варіант в кожного
    //наслідника. Метод получився дуже не зручний, якщо його так реалізувати
    public function filter($params)
    {


$sql=" WHERE ";
foreach($params[1] as $k=>$v){
    $sql.="${params[0]}  {$k}? and ";
}

$len= mb_strlen($sql);
$sql= mb_substr($sql, 0,$len-4);
$this->sql.=$sql;
$this->params= array_values($params[1]);

              return $this;
        
        
    }

    /**
     * @return $this
     */
    public function getCollection()
    {
        $db = new DB();
        $this->sql .= ";";
        $this->collection = $db->query($this->sql, $this->params);
        return $this;
    }

    /**
     * @return mixed
     */
    public function select()
    {
        return $this->collection;
    }

    /**
     * @return null
     */
    public function selectFirst()
    {
        return isset($this->collection[0]) ? $this->collection[0] : null;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getItem($id)
    {
        $sql = "select * from $this->table_name where $this->id_column = ?;";
        $db = new DB();
        $params = array($id);
         return $db->query($sql, $params)[0];
    }

    /**
     * @return array
     */
    public function getPostValues()//отримати значення масива POSТ, при цьому в нього не попаде назва колонки - первичного ключа, 
    
    {
        $values = [];
        $columns = $this->getColumns();
        foreach ($columns as $column) {
            /*
            if ( isset($_POST[$column]) && $column !== $this->id_column ) {
                $values[$column] = $_POST[$column];
            }
             * 
             */
           
            $column_value = filter_input(INPUT_POST, $column);
           
            if ($column_value && $column !== $this->id_column ) {
                $values[$column] = $column_value;
            }

        }
        
        return $values;
    }

    public function getTableName(): string
    {
        return $this->table_name;
    }

    public function getPrimaryKeyName(): string
    {
        return $this->id_column;
    }

    public function setId($id){
        $this->id[]=$id;
    }
    public function getId()
    {
        return $this->id;
    }
    
    





    //перевірка відповідності параметрів для сортування, які приходять через форму до назв стовпців та ASC DESC
    // логіка розбита на 2 функції, щоб можна примінити окремо методи для перевірки відповідності asc/desc а також
    //відповідності назв стовпців наприклад при сортуванні товарів та запису нових товарів
    private function validSortParams($sortparams){
      $sortValues= array_values($sortparams);
      $sortColumns=array_keys($sortparams);
      
      $values = $this->validSortOrderParams($sortValues);
      
      $params=$this->validColumNames($sortColumns);
      
      
      if($values && $params){
          return true;
      }
       return false;

        }
       
       //для перевірки чи відповідають параметри сортування, які отримані з фронтенда  asc/desc
         public  function validSortOrderParams($sortparams){
              foreach($sortparams as $v){
           
           
            //якщо хоча б одно значення в параметрах не відповідає ASC/DESC валідація не пройдена
          
           if ( (strcasecmp('asc', $v) == 0 )  ||  (strcasecmp('desc', $v) == 0) ){
               continue;
                        
            }else{
                return false;
            }
                                      
        }
        return true;
        }
        
        //для перевірки чи відповідають отримані з фронтенда назви колонок колонкам, які містяться в таблиці
        public  function validColumNames($columnNames) {
            foreach($columnNames as $v){
            
            if (!in_array($v, $this->columns)){
               return false;
                               
            }
                                                                  } 
                                                                  return true;
        }
        
    
    }

