<?php
error_reporting(E_ALL);
function d($arr){

	echo "<pre>";
	print_r($arr);
	echo "</pre>";
}

class Validator{
	private $data=[];
    private $rules=[];
    private $errors=[];
   
   
   public function __construct($data,$rules){
   	$this->data=$data;//вхідний масив
   	$this->rules =$rules;//масив прави валідації для вхідного масиву
    
   }

    

    public function run(){
          
          //пройтись по вхідному масиву
          foreach($this->data as $k=>$v){
          	//якщо в масиві з правилами існує такий ключ, як у вхідному масиві, іншими словами, якщо для елемента вхідного масива існує правило(правила) перевірки,
          	if(array_key_exists($k, $this->rules)){
                 
                 //отримати масив із правилами для конкретного елемента первірки
          		$funcArr=explode('|',$this->rules[$k]);
                    
                 //пройтись по ньому, якщо в правилі є аргумент(напр. min:5, що значить визвати функцію min із аргументом 5), то визвати функцію із ним, якщо ні, то визвати функцію без аргумента. Для формування масиву із помилками першим аргументом у всі  методи передаємо ключ вхідного масиву(щоб було ясно по якому полю помилка), другим саме значення вхідного масиву, а третім, якщо є, то додатковий аргумент    
          		foreach($funcArr as $kf=>$func){
                            
          			if(preg_match('#:#', $func)){
          				$pos=strpos($func, ":");
                          
	             $len=(int)substr($func,$pos+1);
	             $func=substr($func, 0,$pos);
	             
	             $this->$func($k,$v,$len);
          			}else{
          				$this->$func($k,$v);
          			}
          		}
          		
          	}
          }
          
          //якщо є помилки, метод поверне false
          return empty($this->errors);
    }

    public function getErrors(){
    	return $this->errors;
    }
   
    private  function min($key,$data,$len){
              if(mb_strlen($data) < $len){
               $this->errors[$key]="Довжина  повинна бути мінімум $len символів";
              }
    }

    private function max($key,$data,$len){
    	if(mb_strlen($data) > $len){
              	$this->errors[$key]="Довжина  повинна бути максимум $len символів";
              }
    }

    private function int($key,$data){
         if(!is_int($data)){
         	$this->errors[$key]="повинно бути ціле число";
         }
    }

    private function float($key,$data){
              if(!is_float($data)){
         	$this->errors[$key]="повинно бути числом з  крапкою";
         }
    }


    private function mail($key,$data){
          $pattern="#[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[a-zA-Z]{2,5}#";
          if(!preg_match($pattern, $data)){
          	$this->errors[$key]="Не валідний адрес електронної пошти";
          }
    }

    private function letters($key,$data){
    	$pattern="#^[^0-9\s%()@?,:;]+$#";
    	if(!preg_match($pattern, $data)){
          	$this->errors[$key]="Повинно містити тільки букви без пробілів";
          };
    }

    //методи відрізняється від інших методів  довжин тим, що перевіряє довжину символів, які приходять з текстових полів
    //без тегів
    private function mintextlen($key,$data,$len){
       $text=strip_tags($data);
       $this->min($key,$text,$len);
    }

    private function maxtextlen($key,$data,$len){
        $text=strip_tags($data);
        $this->max($key,$text,$len);
    }




}




$arr=['name'=>'asdЯч1yuyu','mail'=>'jklj@yu'];
$rules=['name'=>'letters','mail'=>'mail'];
$val= new Validator($arr,$rules);
if(!$val->run()){
  $err=$val->getErrors();
}
$err=$val->getErrors();
 var_dump($err);

//phpinfo();


$rul=[
    'name'=>[
        'minlength'=>10,
        'maxlength'=>15
    ]

];
$er=['name'=>['mioi','jkl']];
$str="<script>12345678</script>";
$res=strip_tags($str);
d(strlen($res));
