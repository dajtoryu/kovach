<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core;

/**
 * Description of Helper
 *
 * @author miha
 */
class Helper {
   public static  function oldInput($k){
       $val=$_SESSION['oldinput'] [$k] ??  '';
       if($val !== ''){
           unset($_SESSION['oldinput'][$k]);
       }
       return $val;
   }
   public static function getCustomer()
   {
        if (!empty($_SESSION['id'])) {
        return self::getModel('customer')->initCollection()
            ->filter(array('customer_id'=>$_SESSION['id']))
            ->getCollection()
            ->selectFirst();
        } else {
            return null;
        }

    }
    
    public static function getModel($name){
        $name = '\\Models\\' . ucfirst($name);
        $model = new $name();
        return $model;
    }
    
    public static  function isAdmin(){
        if(!empty($_SESSION['id'])){
          return (bool) self::getModel('customer')
                  ->initCollection()
                  ->filter(['customer_id'=>$_SESSION['id']])
                  ->getCollection()
                  ->selectFirst()['admin_role'];
        }else{
            return false;
        }
    }

   
   
}
