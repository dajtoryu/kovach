<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Controllers;
use Core\Controller;
use Core\Validator;
/**
 * Description of CustomerController
 *
 * @author miha
 */
class CustomerController extends Controller{
    private $invalid_password;
    
    public function listAction(){
         $this->set('title', "клієнти");
        $customers = $this->getModel('Customer')
            ->initCollection()
            ->sort()
            ->getCollection()
            ->select();
        $this->set('customers', $customers);

        $this->renderLayout();
        
    }
    
     public function LoginAction()
    {
        $this->set('title', "Вхід");
        if (filter_input(INPUT_SERVER, 'REQUEST_METHOD') === 'POST')
        {
            
            
            $email = trim(filter_input(INPUT_POST, 'email'));
            $password = trim(filter_input(INPUT_POST, 'password'));
             $rules=['email'=>'email',
                              'password'=>'req'];
                           
            $aliases=[
                'email'=>'<b>Пошта</b>',
                'password'=>'<b> Пароль </b>',
                            ];
            $data=[
                'email'=>$email,
                'password'=>$password
                
            ];
            
            $validator=new Validator($data, $rules, $aliases);

            if(!$validator->run()){
                 $errors = $validator->getErrors();
                $this->set('errors',$errors);
                $this->setOldInput($data);

            }else{
                  $params =array (
                'email' => $email,
                'password' => md5($password)
                                                    );
                
                  $customer = $this->getModel('customer')->initCollection()
                ->filter($params)
                ->getCollection()
                ->selectFirst();
                
          
                   if(!empty($customer)) {
                $_SESSION['id'] = $customer['customer_id'];
                $this->redirect('/index/index');
            } else {
                $this->invalid_password = 1;
            }
                  
            }
          
            
           
        }
        $this->renderLayout();
    }

    public function registerAction(){
        $this->set('title','реєстрація користувача');
        if (filter_input(INPUT_SERVER, 'REQUEST_METHOD') === 'POST'){
            
            //вирізати пробіли та всі теги
            $data= array_map(function($a){
                return strip_tags(trim($a));
                 
            },$_POST);
            
            
            //правила,  валідації
            $rules=[
                'first_name'=>'letters|min:2',
                'last_name'=>'letters|min:2',
                'phone'=>'regex:\+[0-9]{12}',
                'city'=>'letters|min:2',
                'password'=>'regex:^(?=.*\d)(?=.*[a-z]).{8,}$',
                'confirmpass'=>'equalto:password',
                'email'=>'email'
                               
                
                ];
            $aliases=[
                'first_name'=>'<b>Імя</b>',
                'last_name'=>'<b>Прізвище</b>',
                'phone'=>'<b>Телефон</b>',
                'city'=>'<b>Місто</b>',
                'password'=>'<b>Пароль</b>',
                'confirmpass'=>'<b>Підтвердження пароля</b>',
                'email'=>'<b>Емейл</b>'
                
            ];
            
           
                    //Додатково перевіримо відповідність назв ключів, які отримуємо з POST, очікуваним(наприклад з правил валідації)
            $expectKeys=array_keys($rules);
            $dataKeys=array_keys($data);
            
            //якщо з POST прийшли якісь неочікувані ключі, чи не всі, переадресація назад, на цьому подальше виконання програми завершиться
            if(!empty(array_diff($expectKeys,$dataKeys)) || !empty(array_diff($dataKeys,$expectKeys))){
                 $this->redirect();
            }
            
            
                    $validator  =  new Validator($data,$rules,$aliases);
                    if(!$validator->run()){
                        $errors=$validator->getErrors();
                        $this->set('errors',$errors);
                        $this->setOldInput($data);//вставимо старі значення із форми для виводу в полях
                    }else{
                        
                        //додатково перевіримо наявність користувача з таким емейлом
                        $customerEmail=$this->getModel('customer')
                               ->initCollection()
                              ->filter(['email'=>$data['email']])
                              ->getCollection()
                              ->selectFirst();
                        if($customerEmail !== null){//якщо такий користувач існує
                            $this->set('errors',['mail'=>'Користувач з таким емейлом вже зареєстрований']);
                            $this->setOldInput($data);
                        }else{
                            
                            //захешуємо пароль
                            $data['password']=md5($data['password']);
                            //заберемо лишнє поле підтвердження емейла
                            unset($data['confirmpass']);
                            $customer=$this->getModel('customer')
                                      ->addItem($data);
                             if(!empty($customer)) {
                $_SESSION['id'] = $customer;
                $this->redirect('/index/index');
            } else {
                $this->invalid_password = 1;
            }
                                                       
                        }
                    }
           
            
        }
        
        
        
        
      $this->renderLayout();    
    }
    
    
    public function LogoutAction()
    {

        $_SESSION = [];

        // expire cookie

        if (!empty($_COOKIE[session_name()]))
        {
            setcookie(session_name(), "", time() - 3600, "/");
        }

        session_destroy();
        $this->redirect('/index/index');
    }

    public function roomAction(){
        $customer= \Core\Helper::getCustomer();
        if($customer){
                    $this->set('title','Особистий кабінет');
    
            $model=$this->getModel('customer');
            $res=$model->getSales($customer['customer_id']);
            $this->set('sales',$res);
             $this->renderLayout();

        }else{
            $this->redirect();
        }
    }
// ...
    
}
