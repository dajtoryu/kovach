<?php
namespace Controllers;

use Core\Controller;
use Core\View;
use Core\Validator;
use Core\Session;
use Core\Helper;
/**
 * Class ProductController
 */
class ProductController extends Controller
{
    public function indexAction()
    {
        $this->forward('product/list');
    }

    /**
     *
     */
   
    public function listAction($perpage=null)
    {
       $admin = Helper::isAdmin();
        $this->set('title', "Товари");
        $product_model = $this->getModel('Product');    //отримати модель, при цьому назва таблиці з якою вона працює визначається в конструкторі, там же вказується імя первичного ключа 
        $max_price=$product_model->getMaxPrice();//максимальна ціна товара для фільтра
        
        


        
       
        $products = $product_model
            ->initCollection()//отримати назви колонок таблиці із БД, сформувати строку запита в якій містяться назви всіх колонок select col1,col2 і т.д.
              ->filter($this->getFilterPriceParams($max_price))                                   
            ->sort($this->getSortParams())//дописати в строку запиту порядок сортування, залежно від такого в  POST
            ->getCollection( )//безпосереднє отримання інформ із БД на підставі сформованої строки запиту, результати помістити в свойство моделі, 2 параметри в методі - поточна сторінка та кількість записів для виводу
            ->select();//повернути результат запиту - колекцію-свойство моделі
           
        $this->set('maxprice',$max_price);
        $this->set('products', $products);
        $this->set('admin',$admin);
        $this->renderLayout();
    }

    /**
     *
     */
    public function viewAction()
    {
        $this->set('title', "Карточка товара");
        $product = $this->getModel('Product')
            ->initCollection()
            ->filter([0=>'id',1=>['='=>$this->getId()]])
            ->getCollection()
            ->selectFirst();
        $this->set('products', $product);

        $this->renderLayout();
    }

    /**
     *
     */
    public function editAction()
    {
        
        $admin=Helper::isAdmin();
        if($admin){
                   
            
        
        $model = $this->getModel('Product');
        $this->set('saved', 0);
        $this->set("title", "Редагування товару");
        $id = filter_input(INPUT_POST, 'id');//id яка передається POST  для запису редагування, зчитується із скритого поля форми
       
        if ($id) {
            $idisset=$model->getItem($id);
            if(!$idisset){//якщо товара з таким id не існує, перенаправити, на цьому скрипт завершиться
                $this->redirect();
            }
            $values = $model->getPostValues();

            
            //правила залишення тегів для  поля описання товару:
            $par=['description'=>'<ul><li><b></b><i></i><ol><strong><p><em>'];
            $prepare_values=$this->cleanPostValues($values, $par);
           
            $rules=['sku'=>'req',
                            'name'=>'min:4|max:256',
                            'price'=>'number',
                            'qty'=>'number',
                            'description'=>'mintextlen:10'
                         ];
            $aliases=[
                'sku'=>'<b>Артикул</b>',
                'name'=>'<b>Назва товару</b>',
                'price' =>'<b>Ціна</b>',
                'qty'=>'<b>Кількість</b>',
                'description'=>'<b>Описання товару</b>',
                
            ];
            $validator = new Validator($prepare_values,$rules,$aliases);
            if(!$validator->run()){
                $errors = $validator->getErrors();
                $this->set('errors',$errors);
               
            }else{
                 //додатково перекодували html в описанні товара
               if(isset($values['description'])){
                 $prepare_values['description'] = htmlspecialchars($prepare_values['description']);
                                 
                                                                                }
            $this->set('saved', 1);
            $res=$model->saveItem($id,$prepare_values);
            if(!$res){
                $this->redirect('/product/list');
            }
            }
            
        }
        
        $product= $model->getItem($this->getId());//отримати продукт із БД
        //якщо не існуючий id, перенаправити або на попередню сторінку, або на головну
        if(!$product){
            //header('Location:https://cyberpolice.gov.ua');//або на сайт кіберполіції
            //die;
            $this->redirect();
        }else{
            $this->set('product', $product);  
            $this->renderLayout();
        }
    }else{
        
        $this->redirect();
    }
      
    }

    /**
     *
     */
    public function addAction()
    {
        $admin= Helper::isAdmin();
       
        if($admin){
              $model = $this->getModel('Product');
        $this->set("title","Додавання товару");

        if ($values = $model->getPostValues()) {//може бути присвоєно?
          //оскільки метод getPostValues() не повертає поля із POST, які приходять пустими, то у валідаторі, звіряються ключі, які прийшли
            //з POST із ключами у правилах валідації, формується повідомлення про помилку - поле не заповнене.  Таким чином, якщо прийде 
            //поле пусте, або не прийде ключ із POST, який передбачено у прпавилах валідації виведеться одне і теж повідомлення-поле не заповнене
  
            
            
            //метод для очистки полів POST від тегів, для  description задаємо правила очистки
            $par=['description'=>'<ul><li><b></b><i></i><ol><strong><p><em>'];
            
            $prepare_values=$this->cleanPostValues($values, $par);
// -----------------  валідація ------------------------
            //задамо правила валідації;
            $rules=['sku'=>'req',
                            'name'=>'min:4|max:256',
                            'price'=>'number',
                            'qty'=>'number',
                            'description'=>'mintextlen:10'];
            $aliases=[
                'sku'=>'<b>Артикул</b>',
                'name'=>'<b>Назва товару</b>',
                'price' =>'<b>Ціна</b>',
                'qty'=>'<b>Кількість</b>',
                'description'=>'<b>Описання товару</b>'
            ];
            $validator = new Validator($prepare_values,$rules,$aliases);
            if(!$validator->run()){
                $errors = $validator->getErrors();
                $this->set('errors',$errors);
                $this->setOldInput($prepare_values);
            }else{
                
                //додатково перекодували html в описанні товара
               if(isset($values['description'])){
                 $prepare_values['description'] = htmlspecialchars($prepare_values['description']);
                
                 
               }
               
                   //метод запису даних в БД, визначений не в базовій, а в моделі продукта, крім того в Core\DB добавлено окремий метод для вставки
               //даних, який повертає id вставленого товара, що необхідно для редиректа на сторінку редагування останнього згідно умов задачі 
                  $id=$model->addItem($prepare_values);
                  $redirect='/product/edit?id='.$id."&s=".$prepare_values['name'];
                  $this->redirect($redirect);//при запису в БД перенаправили на сторінку редагування товару
                 
            }
            
                     
                             
    }
            
            
            
        }else{
            $this->redirect();
        }
      
        
    $this->renderLayout();

    }
    /**
     * @return array
     */
    
    public function deleteAction(){
        $admin= Helper::isAdmin();
        
        if($admin){
                      $model = $this->getModel('Product');

            //        $id = filter_input(INPUT_POST, 'id');//id яка передається POST  для запису редагування, зчитується із скритого поля форми
        //або так, через гет-параметер
          $id=$this->getId();
        if ($id) {
            $idisset=$model->getItem($id);//чи  існує товар з таким id
            
           
            if(!$idisset){//якщо товара з таким id не існує, перенаправити, на цьому скрипт завершиться
                $this->redirect();
        }else{
             $model->setId($id);//записати в модель ід
            $model->deleteItem();
            $this->redirect('/product/list');
        }
        
        
            }
            
        }else{
            
            $this->redirect();
        }
        
          

    }
   
    
    
    //бере значення для сортування з кукі або  з POST, записує в кукі, також передає масив значень для формування select на фронті 
    public function getSortParams()  {
        
            
            $params = [];
            
            
        $sortfirst = filter_input(INPUT_POST, 'sortfirst');
        $sortsecond= filter_input(INPUT_POST, 'sortsecond');
        if(!$sortfirst  &&  !$sortsecond){//якщо  метод GET(одночасно для обох, оскільки, як я розумію одночасно GET i POST, не може бути), взяти із куки, якщо існує
            if(isset($_COOKIE['product_sort'])){
                $par= unserialize($_COOKIE['product_sort']);
                $params['price']=$par['price'];
                $params['qty']=$par['qty'];
            }else{//якщо нема в кукі, то за замовчуванням
                $params['price']='ASC';
                $params['qty'] = 'ASC';
            }
        }else{//якщо методом POST, заповнити відповідно  масив параметрів та записати в куку 
            if($sortfirst === "price_DESC"){
                $params['price']="DESC";
            }else{
                $params['price']="ASC";
            }
            
            if($sortsecond == "qty_DESC"){
                $params['qty']="DESC";
            }else{
               $params['qty']="ASC";
                               
            }
            $pars= serialize($params);
            setcookie('product_sort',$pars,time()+3600);
            
        }
//        if ($sortfirst === "price_DESC") {
//            $params['price'] = 'DESC';
//        } else {
//            $params['price'] = 'ASC';
//        }
//        $sortsecond = filter_input(INPUT_POST, 'sortsecond');
//        if ($sortsecond === "qty_DESC") {
//            $params['qty'] = 'DESC';
//        } else {
//            $params['qty'] = 'ASC';
//        }
        //Одночасно формуємо масив даних для виводу на сторінці вибраного selecta
        $this->set('selected',$this->setSelectedValues($params));
        return $params;
        
        

    }
    
    //отримати параметри для фільтрації ціни Формується масив виду: [0=>'price'1=>['>='=>'цінавід','<='=>'цінадо']]
    public function getFilterPriceParams($maxprice){
        $regex='#^[0-9]+\.?[0-9]*$#';//якщо не число,  без  крапки або відємне
        $params[]='price';
        
                    $pricefrom= filter_input(INPUT_POST, 'pricefrom',FILTER_VALIDATE_REGEXP,['options'=>['regexp'=>'#^[0-9]+\.?[0-9]*$#']]);
                   $priceto=filter_input(INPUT_POST, 'priceto',FILTER_VALIDATE_REGEXP,['options'=>['regexp'=>'#^[0-9]+\.?[0-9]*$#']]);
        
                   
        if($pricefrom != false){
            $params[1]['>=']=$pricefrom;

        }else{
          
                $params[1]['>']=0;
           
        }
        
        
        if($priceto != false){
            $params[1]['<=']=$priceto;

        }else{
                 $params[1]['<=']=$maxprice;
        
        }
        return $params;
    }

    /**
     * @return array
     */
    public function getSortParams_old()
    {
        /*
        if (isset($_GET['sort'])) {
            $sort = $_GET['sort'];
        } else 
        { 
            $sort = "name";
        }
         * 
         */
        $sort = filter_input(INPUT_GET, 'sort');
        if (!isset($sort)) {
            $sort = "name";
        }
        /*
        if (isset($_GET['order']) && $_GET['order'] == 1) {
            $order = "ASC";
        } else {
            $order = "DESC";
        }
         * 
         */
        if (filter_input(INPUT_GET, 'order') == 1) {
            $order = "DESC";
        } else {
            $order = "ASC";
        }
        
        return array($sort, $order);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        /*
        if (isset($_GET['id'])) {
         
            return $_GET['id'];
        } else {
            return NULL;
        }
        */
        return filter_input(INPUT_GET, 'id');
    }
    
    //отримати поточну сторінку для пагінації
    //якщо в гет-параметрах є ключ р і він є числовим та більшим  0, то повернути його в іншому випадку по вернути 1
    protected function getPage(){
        if(isset($_GET['p'])  &&  preg_match('#^[0-9]+$#',$_GET['p'])  &&  $_GET['p']>0){
            return (int) $_GET['p'];
        } else{
            return 1;
        } 
        
        
    }
    
    //функція для створення параметрів виборки результата для формування пагінації
    protected function createLimit($page,$count,$limit){
        //визначити  кількість сторінок аби не перейти на записи яких не існує
        $pages=intval($count/$limit);
        $remainder=$count%$limit;
        if($remainder>0) $pages++;
        //якщо в гет-параметрах неіснуюча сторінка, то перейти на останню
        if($page>$pages) $page=$pages;
        
        //визначити запис з якої стартуємо
        $from=$page*$limit - $limit;
        $to =$from+ $limit-1;//доки крутимо цикл масива
        if($to>=$count){//щоб не вийти потім за межі масива
            $to=$count-1;
        }
        $this->set('pagination',[$page,$pages]);//записали у внутрішнє свойство контроллера дані, які нам необхідні для формування розмітки пагінації
        $result=['current_page'=>$page,'pages'=>$pages,'limit'=>[$from,$to]];
        
        return $result;
        
    }
     //метод перенесено в базовий контроллер
//    //очищає від тегів, залишає дозаолені тегі в $params $exclude - масив ключів, в елементах яких не потрібно нічого вирізати
//    protected  function cleanPostValues($values,$params,$exclude=[]){
//        $prepare_values=[];
//        foreach($values as $k=>$v){
//            //якщо є ключ який не потрібно обрізати,  добавити його в масив і пропустити ітерацію
//            if(!empty($exclude) &&  in_array($k, $exclude)){
//                $prepare_values[$k]=$v; continue;
//            }   
//             
//            
//            //якщо в параметрах є правило для тегів, а також якщо в $values є такий ключ, оскільки getPostValues() не поверне ключі-значення пустих полів 
//            if(array_key_exists($k, $params) && array_key_exists($k, $values)){
//                //примінити правило дозволених тегів до таких полів
//                $prepare_values[$k]= strip_tags($v,$params[$k]);
//            }else{//якщо правила для поля не встановлено, то вирізати всі теги
//                $prepare_values[$k]= strip_tags($v);
//                
//            }
//            
//        }
//        return $prepare_values;
//        
//    }
    
    public function arrayPaginate($arr,$params){
        $arrayPice = [];
        $from=$params['limit'][0];
        $to=$params['limit'][1];
        for($i=$from;$i<=$to;$i++){
            $arrayPice[]=$arr[$i];
            
        }
        return $arrayPice;
    }
    
    public function backAction(){
        $this->redirect('/product/list');
    }
    
    protected function setOldInput($param) {
        foreach($param as $k=>$v){
                    $_SESSION['oldinput'][$k]=$v;

        }
    }
    
    //значення для підстановки в select залежно від параметрів сортування  на сторінці списку товарів 
    protected function setSelectedValues($params){
        $sel=[];
        foreach($params as $k=>$v){
            if($v === 'ASC'){
                $sel[$k]['ASC']='selected';
                $sel[$k]['DESC']='';
            }elseif($v === 'DESC'){
                $sel[$k]['DESC']='selected';
                $sel[$k]['ASC']='';
            }
        }
        
        return $sel;
    }
    
    
    
    
}