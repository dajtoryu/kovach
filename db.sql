-- Adminer 4.8.0 MySQL 5.5.5-10.3.27-MariaDB-0+deb10u1 dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `customer`;
CREATE TABLE `customer` (
  `customer_id` int(11) NOT NULL AUTO_INCREMENT,
  `last_name` varchar(255) NOT NULL DEFAULT '',
  `first_name` varchar(255) NOT NULL,
  `telephone` varchar(13) NOT NULL,
  `email` varchar(255) NOT NULL,
  `city` varchar(50) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `admin_role` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`customer_id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `customer` (`customer_id`, `last_name`, `first_name`, `telephone`, `email`, `city`, `password`, `admin_role`) VALUES
(1,	'Півкач',	'Михайло',	'+380501111111',	'mykhailo.pivkach@transoftgroup.com',	'Мукачево',	'3fc0a7acf087f549ac2b266baf94b8b1',	1),
(2,	'test',	'test',	'123',	'test@gmail.com',	'test',	'e20b5cdd013d652e5218da9ca0029c0a',	0),
(3,	'testtest',	'testdtest',	'0313137862',	'testtest@gmail.com',	'Mukachevo',	'e6c1ef25b5bcaaacc285489eae10d5e1',	0),
(4,	'www',	'tttt',	'654605465465',	'www@tt.com',	'Mukachevo',	'3ade3fd6e8eef84f2ea91f6474be10d9',	0),
(5,	'tttwww',	'tttwww',	'564545',	'eee@ttt.com',	'3fc0a7acf087f549ac2b266baf94b8b1',	'eeee',	0);

DROP TABLE IF EXISTS `customers`;
CREATE TABLE `customers` (
  `customer_id` int(11) NOT NULL AUTO_INCREMENT,
  `last_name` varchar(255) NOT NULL DEFAULT '',
  `first_name` varchar(255) NOT NULL,
  `phone` varchar(13) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `city` varchar(50) NOT NULL,
  `admin_role` int(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`customer_id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `customers` (`customer_id`, `last_name`, `first_name`, `phone`, `email`, `password`, `city`, `admin_role`) VALUES
(2,	'Півкач',	'Михайло',	'+380501111111',	'mykhailo.pivkach@transoftgroup.com',	'3fc0a7acf087f549ac2b266baf94b8b1',	'Мукачево',	1),
(13,	'Murff',	'Donald',	'+443125911482',	'murf@microsoft.com',	'440655d6d98f244c507d27576a9209c5',	'Chicago',	0),
(14,	'Kovach',	'Michael',	'+56788899',	'sdfg@qwer.ru',	'827ccb0eea8a706c4c34a16891f84e7b',	'Ungvar',	0),
(15,	'Duck',	'Donald',	'898989889898',	'donald@duck.ru',	'5f4dcc3b5aa765d61d8327deb882cf99',	'LA',	0);

DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `path` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `sort_order` smallint(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `menu` (`id`, `name`, `path`, `active`, `sort_order`) VALUES
(1,	'Товари',	'/product/list',	1,	1),
(2,	'Клієнти',	'/customer/list',	1,	2),
(3,	'Тест',	'/test/test',	1,	3);

DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nameOrd` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `telephone` bigint(15) NOT NULL,
  `sku` varchar(30) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` decimal(12,2) NOT NULL,
  `date_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `orders` (`id`, `nameOrd`, `email`, `telephone`, `sku`, `name`, `price`, `date_at`) VALUES
(1,	'Михайло',	'mykhailo.pivkach@transoftgroup.com',	313137862,	'фывфывыф',	'<h1>выфвфыв</h1>',	0.00,	'2019-11-21 10:33:46');

DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sku` varchar(30) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` decimal(12,2) NOT NULL DEFAULT 0.00,
  `qty` decimal(12,3) NOT NULL DEFAULT 0.000,
  `description` mediumtext DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `products` (`id`, `sku`, `name`, `price`, `qty`, `description`) VALUES
(2,	't00002',	'Телефон 2',	6696.00,	4.000,	''),
(3,	't00003',	'Телефон 3',	10798.80,	3.000,	'<h1>Телефон 3</h1>'),
(4,	't00004',	' <h1>Телефон 4</h1> ',	5880.00,	5.000,	' &lt;h1&gt;Телефон 4&lt;/h1&gt; '),
(6,	't00005',	'Телефон 5',	5881.00,	6.000,	''),
(7,	't00006',	'Телефон6',	5880.00,	3.000,	NULL),
(13,	't00098',	'Новий телефон',	1234.00,	3.000,	'<p>НОвий <strong>Телефон</strong></p>\r\n'),
(14,	't00089',	'LG',	2500.00,	12.000,	'<p>Телефон <strong>LG</strong></p>\r\n'),
(15,	't67676',	'Iphone',	23000.00,	12.000,	'<p>Iphone</p>\r\n'),
(16,	't6786',	'Iphone12',	23456.00,	12.000,	'<p>Iphone12</p>\r\n'),
(17,	't6786',	'Iphone12',	23456.00,	18.000,	'&lt;p&gt;Iphone12hjhkjhkjh&lt;/p&gt;'),
(18,	't6787',	'Samsung',	12000.00,	3.000,	'<p>Samsung</p>\r\n'),
(19,	't4545',	'Nokia',	1220.00,	4.000,	'<p>Nokia</p>\r\n'),
(20,	't6785',	'Sony',	12345.00,	5.000,	'<p>Sony</p>\r\n'),
(22,	'е6786',	'Ericsson',	2345.00,	4.000,	'<p>Ericsson</p>\r\n'),
(23,	't45444',	'Sony222',	347.00,	12.000,	'&lt;p&gt;&lt;em&gt;&lt;strong&gt;jjlkjlkjljlkjlkjljlkjl&lt;/strong&gt;&lt;/em&gt;&lt;/p&gt;'),
(24,	't45444',	'Sony2',	345.00,	12.000,	'&lt;p&gt;Sony2Sonyyyyy&lt;/p&gt;'),
(75,	'rthjrt',	'tnrtn',	3456.00,	4546.000,	'&lt;p&gt;gffngfnfgnfgn&lt;/p&gt;'),
(76,	'erytr',	'trhrhrth',	23245.00,	12.000,	'&lt;p&gt;rerererertererf&lt;/p&gt;');

DROP TABLE IF EXISTS `sales_order`;
CREATE TABLE `sales_order` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`order_id`),
  KEY `FK_ORDER_CUSTOMER` (`customer_id`),
  CONSTRAINT `sales_order_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`customer_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `sales_order` (`order_id`, `customer_id`, `datetime`) VALUES
(1,	1,	'2019-10-24 15:29:59'),
(2,	1,	'2019-10-24 15:31:17'),
(3,	1,	'2019-10-24 15:44:08'),
(4,	1,	'2019-10-24 15:44:10');

DROP TABLE IF EXISTS `sales_orderitem`;
CREATE TABLE `sales_orderitem` (
  `orderitem_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) unsigned NOT NULL,
  `qty` decimal(12,3) NOT NULL,
  PRIMARY KEY (`orderitem_id`),
  KEY `FK_ORDERITEM_ORDER` (`order_id`),
  KEY `FK_ORDERITEM_PRODUCT` (`product_id`),
  CONSTRAINT `sales_orderitem_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `sales_order` (`order_id`) ON DELETE CASCADE,
  CONSTRAINT `sales_orderitem_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `sales_orderitem` (`orderitem_id`, `order_id`, `product_id`, `qty`) VALUES
(2,	1,	2,	1.000),
(3,	2,	3,	1.000),
(4,	2,	4,	2.000);

DROP TABLE IF EXISTS `shopping`;
CREATE TABLE `shopping` (
  `orderitem_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `product_id` int(11) unsigned NOT NULL,
  `qty` int(11) NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT current_timestamp(),
  `result` tinyint(4) NOT NULL,
  PRIMARY KEY (`orderitem_id`),
  KEY `FK_ORDERITEM_ORDER` (`customer_id`),
  KEY `FK_ORDERITEM_PRODUCT` (`product_id`),
  CONSTRAINT `shopping_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`customer_id`) ON DELETE CASCADE,
  CONSTRAINT `shopping_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `shopping` (`orderitem_id`, `customer_id`, `product_id`, `qty`, `datetime`, `result`) VALUES
(2,	1,	2,	1,	'2019-11-21 10:53:37',	1),
(3,	1,	4,	1,	'2019-11-21 10:53:59',	1);

DROP TABLE IF EXISTS `test`;
CREATE TABLE `test` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `test` (`id`, `name`, `date`) VALUES
(1,	'test',	'2019-10-22 18:44:59');

-- 2021-06-07 17:54:40

